//
//  RecommendationCell.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "RecommendationCell.h"

@implementation RecommendationCell

- (void) setRecommendCell:(RecommendEntity *) _recommend {
    
    self.lblName.text = [NSString stringWithFormat:@"%@ %@", _recommend._fName,  _recommend._lName];
    self.lblDescription.text = _recommend._content;
}

@end
