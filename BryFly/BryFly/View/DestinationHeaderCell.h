//
//  DestinationHeaderCell.h
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DestinationHeaderCell : UITableViewCell
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lblName;

@end
