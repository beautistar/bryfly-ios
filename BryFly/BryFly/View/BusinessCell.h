//
//  BusinessCell.h
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtils.h"


@interface BusinessCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvPicture;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;

- (void) setBusinessCell:(BusinessEntity *) _business;

@end
