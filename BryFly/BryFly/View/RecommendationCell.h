//
//  RecommendationCell.h
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtils.h"

@interface RecommendationCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

- (void) setRecommendCell:(RecommendEntity *) _recommend;

@end
