//
//  BusinessCell.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "BusinessCell.h"

@implementation BusinessCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setBusinessCell:(BusinessEntity *) _business {
    
    self.lblName.text = _business._name;
    self.lblDistance.text =  _business._category;
    self.lblPhone.text = _business._contact;
    [self.imvPicture setImageWithURL:[NSURL URLWithString:_business._logo]];
    
}
@end
