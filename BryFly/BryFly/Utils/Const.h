//
//  Const.h
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Const : NSObject

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

// string const

#define SAVE_ROOT_PATH                      @"BryFly"

#define CONN_ERROR                          @"Connect server fail.\n Please try again."

#define ALERT_TITLE                         @"BryFly"
#define ALERT_OK                            @"OK"
#define ALERT_CANCEL                        @"Cancel"
#define INPUT_EMAIL                         @"Please enter your Email address."
#define VALID_EMAIL                         @"Please enter valid Email address"
#define VALID_PWD                           @"Password must be at least 6 characters"
#define INPUT_USERFNAME                     @"Please enter your first name."
#define INPUT_USERLNAME                     @"Please enter your last name."
#define INPUT_PWD                           @"Please enter your password."
#define NEED_PURCHASE                       @"Please make purchase."
#define ACCEPT_TERM                         @"Please accept Pricvacy Policy & Terms of Use."

#define EXIST_USERNAME                      @"User name already exist."
#define EXIST_EMAILADDRESS                  @"Email address already exist."
#define UNREGISTERED_EMAIL                  @"Unregistered email address."
#define WRONG_PASSWORD                      @"Wrong password."

#define INPUT_BUSINESSNAME                  @"Please enter business name."
#define INPUT_DESCRIPTION                   @"Please enter business description."
#define INPUT_CONTACTINFO                   @"Please enter contact info."
#define SELECT_CATEGORY                     @"Please select category."
#define SELECT_STATE                        @"Please select state."
#define SELECT_CITY                         @"Please select city."
#define INPUT_SPECIAL_OFFER                 @"Please enter special offer."
#define SELECT_LOGO                         @"Please select company logo."

#define INPUT_RECOMMEND                     @"Please enter recommendation."

#define RENEW_PURCHASE_MONTHLY              @"Basic Plan\n\nMonthly auto renewing subscription - Unlimited app access at $9.99/mo.\nPayment will be charged to iTunes Account at confirmation of purchase.\nSubscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period.\nAccount will be charged for renewal within 24-hours prior to the end of the current period at $9.99.\nSubscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase."

#define RENEW_PURCHASE_YEARLY               @"Premium Plan\n\nAnnually auto renewing subscription- Unlimited app access at $99.99/yr.\nPayment will be charged to iTunes Account at confirmation of purchase. Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period.\nAccount will be charged for renewal within 24-hours prior to the end of the current period at $99.99. Subscriptions may be managed by the user and auto-renewal may be turned off by going to the user's Account Settings after purchase."

#define UPDATE_FAIL                         @"FAILED."
#define SUCCESS_UPDATE                      @"SUCCESS."
#define SUCCESS_EDIT                        @"SUCCESS."
#define SUCCESS_RECOMMEND                   @"SUCCESS."
#define SUCCESS_UPDATED                     @"SUCCESS."
#define SUCCESS_RESETPWD                    @"SUCCESS."
#define EXIST_BUSINESSNAME                  @"Bsuiness name already exist."
#define COMPLETE_BUSINESS                   @"Please complete your business profile."
#define CANT_RECOMMEND                      @"Business user can't add recommendation."

// Integer const
    
#define FROM_PILOT                          111
#define FROM_FILGHT                         222
#define FROM_BUSINESS                       333

#define FROM_LIST                           444
#define FROM_RECOMMEND                      555

#define BUSINESS_USER                       1
#define COMMON_USER                         0

extern int addedRecommend;
extern int updatedBusiness;



@end
