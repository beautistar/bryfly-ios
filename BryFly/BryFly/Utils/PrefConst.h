//
//  PrefConst.h
//  BryFly
//
//  Created by Developer on 12/8/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_USERID                      @"userid"
#define PREFKEY_USEREMAIL                   @"user_email"
#define PREFKEY_USERPASSWORD                @"user_password"
#define PREFKEY_USERFNAME                   @"user_fname"
#define PREFKEY_USERLNAME                   @"user_lname"
#define PREFKEY_USERNAME                    @"user_name"
#define PREFKEY_USERREMEMBER                @"user_remember"
#define PREFKEY_USERTYPE                    @"user_type"

#endif /* PrefConst_h */
