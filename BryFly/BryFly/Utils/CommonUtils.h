//
//  CommonUtils.h
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "Const.h"
#import "NSString+Encode.h"
#import "ReqConst.h"
#import "PrefConst.h"
#import "UserEntity.h"
#import "AppDelegate.h"
#import "RecommendEntity.h"

@import AFNetworking;
@import Toaster;

@interface CommonUtils : NSObject

+ (BOOL) isValidEmail: (NSString *) email;

// save image to file with size specification
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile;

+ (void) saveUserInfo;
+ (void) loadUserInfo;

+ (void) setUserFName : (NSString *) fname;
+ (NSString *) getUserFName;

+ (void) setUserLName : (NSString *) lname;
+ (NSString *) getUserLName;

+ (void) setUserName : (NSString *) fullname;
+ (NSString *) getUserName;

+ (void) setuserIdx : (int) idx;
+ (int) getUserIdx;

+ (void) setUserEmail : (NSString *) email;
+ (NSString *) getUserEmail;

+ (void) setUserPassword : (NSString *) password;
+ (NSString *) getUserPassword;

+ (void) setuserType:(int) userType;
+ (int) getUserType;

+ (void) setUserRemember : (BOOL ) remember;
+ (BOOL) getUserRemember;

@end
