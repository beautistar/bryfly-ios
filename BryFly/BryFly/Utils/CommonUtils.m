//
//  CommonUtils.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CommonUtils.h"
#import "UIImageResizeMagick.h"
#import "UserDefault.h"

@implementation CommonUtils

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
    }
    
    return self;
}

+ (BOOL) isValidEmail: (NSString *) email {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

// resize image with specified size
+ (UIImage *) imageResize: (UIImage *) srcImage {
    
    return [srcImage resizedImageByMagick:@"128x128#"];
}

+ (UIImage *) imageResizeforChat: (UIImage *) scrImage {
    
    return [scrImage resizedImageByMagick:@"256x410#"];
}


// save image to file (Documents/Campus/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    if(isProfile) {
        
        outputFileName = @"profile.jpg";
        outputImage = [CommonUtils imageResize:srcImage];
        
    }
    else {
        
        outputFileName = @"chatfile.jpg";
        outputImage = [CommonUtils imageResizeforChat:srcImage];
    }
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:SAVE_ROOT_PATH withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:SAVE_ROOT_PATH];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
    //    if(isProfile) {
    [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
    //    } else {
    //        [UIImageJPEGRepresentation(outputImage, 0.8) writeToFile:filePath atomically:YES];
    //    }
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}

+ (void) saveUserInfo {
    
    [CommonUtils setuserIdx:APPDELEGATE.Me._idx];
    [CommonUtils setUserFName:APPDELEGATE.Me._fName];
    [CommonUtils setUserName:APPDELEGATE.Me._lName];
    [CommonUtils setUserName:APPDELEGATE.Me._fullName];
    [CommonUtils setUserEmail:APPDELEGATE.Me._email];
    [CommonUtils setUserPassword:APPDELEGATE.Me._password];
    [CommonUtils setUserRemember:APPDELEGATE.Me._remember];
    [CommonUtils setuserType:APPDELEGATE.Me._userType];
}

+ (void) loadUserInfo {

    if (APPDELEGATE.Me) {
        
        APPDELEGATE.Me._idx = [CommonUtils getUserIdx];
        APPDELEGATE.Me._fName = [CommonUtils getUserFName];
        APPDELEGATE.Me._lName = [CommonUtils getUserLName];
        APPDELEGATE.Me._fullName = [CommonUtils getUserName];
        APPDELEGATE.Me._email = [CommonUtils getUserEmail];
        APPDELEGATE.Me._password = [CommonUtils getUserPassword];
        APPDELEGATE.Me._remember = [CommonUtils getUserRemember];
        APPDELEGATE.Me._userType = [CommonUtils getUserType];
    }
    
}

+ (void) setUserFName : (NSString *) fname {
    
    [UserDefault setStringValue:PREFKEY_USERFNAME value:fname];
}

+ (NSString *) getUserFName {
    
    return [UserDefault getStringValue:PREFKEY_USERFNAME];
    
}

+ (void) setUserLName : (NSString *) lname {
    
    [UserDefault setStringValue:PREFKEY_USERLNAME value:lname];
}
+ (NSString *) getUserLName {
    
    return [UserDefault getStringValue:PREFKEY_USERLNAME];
}

+ (void) setUserName : (NSString *) fullname {
    
    [UserDefault setStringValue:PREFKEY_USERNAME value:fullname];
}
+ (NSString *) getUserName {
    return [UserDefault getStringValue:PREFKEY_USERNAME];
}

+ (void) setuserIdx : (int) idx {
    
    [UserDefault setIntValue:PREFKEY_USERID value:idx];
}
+ (int) getUserIdx {
    
    return [UserDefault getIntValue:PREFKEY_USERID];
}

+ (void) setUserEmail : (NSString *) email {
    
    [UserDefault setStringValue:PREFKEY_USEREMAIL value:email];
}
+ (NSString *) getUserEmail {
    
    return [UserDefault getStringValue:PREFKEY_USEREMAIL];
}

+ (void) setUserPassword : (NSString *) password {
    
    [UserDefault setStringValue:PREFKEY_USERPASSWORD value:password];
}
+ (NSString *) getUserPassword {
    
    return [UserDefault getStringValue:PREFKEY_USERPASSWORD];
}

+ (void) setUserRemember : (BOOL ) remember {
    
    [UserDefault setBoolValue:PREFKEY_USERREMEMBER value:remember];
}
+ (BOOL) getUserRemember {
    
    return [UserDefault getBoolValue:PREFKEY_USERREMEMBER];
}

+ (void) setuserType:(int) userType {
    [UserDefault setIntValue:PREFKEY_USERTYPE value:userType];
}

+ (int) getUserType {
    
    return [UserDefault getIntValue:PREFKEY_USERTYPE];
    
}

@end
