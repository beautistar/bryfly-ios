//
//  ReqConst.h
//  BryFly
//
//  Created by Developer on 12/8/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#ifndef ReqConst_h
#define ReqConst_h

/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

#define SERVER_BASE_URL                 @"http://35.161.165.155"

#define SERVER_URL [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, @"/index.php/api/"]

#define REQ_SIGNUP                      @"signup"
#define REQ_LOGIN                       @"login"
#define REQ_RESETPWD                    @"resetPassword"
#define REQ_CREATEBUSINESS              @"createBusiness"
#define REQ_UPDATEBUSINESS              @"updateBusiness"
#define REQ_UPDATEBUSINESSLOGO          @"updateBusinessLogo"
#define REQ_ADDRECOMMEND                @"addRecommend"
#define REQ_GETBUSINESS                 @"getBusiness"
#define REQ_UPDATERECOMMEND             @"updateRecommend"
#define REQ_DELETERECOMMEND             @"deleteRecommend"

#pragma mark - 
#pragma mark - Parameters

#define IDX                             @"idx"
#define EMAIL                           @"email"
#define F_NAME                          @"f_name"
#define L_NAME                          @"l_name"
#define USER_TYPE                       @"user_type"
#define USER_ID                         @"user_id"


#define NAME                            @"name"
#define DESCRIPTION                     @"description"
#define CONTACT                         @"contact"
#define CATEGORY                        @"category"
#define STATE                           @"state"
#define CITY                            @"city"
#define SPECIAL_OFFER                   @"special_offer"
#define LOGO_URL                        @"logo_url"

#define CONTENT                         @"content"


#pragma mark -
#pragma mark - Response Params

#define RES_CODE                        @"result_code"
#define RES_USERINFO                    @"user_info"
#define ID                              @"id"
#define USER_ID                         @"user_id"
#define FILE_URL                        @"file_url"

#define RES_BUSINESSINFO                @"business_info"
#define RES_RECOMMENDINFO               @"recommend_info"



/**
 **     Response Code
 **/

#pragma mark -
#pragma mark - Response Code

#define CODE_SUCCESS                    0
#define CODE_USERNAMERXIST              201
#define CODE_EMAILEXSIT                 202
#define CODE_UNREGISTEREDUSER           203
#define CODE_WRONGPWD                   204
#define CODE_EXISTBUNAME                205
#define CODE_UPLOADLOGOFAIL             206
#define CODE_NON_COMPLETED_BUSINESS     207


#endif /* ReqConst_h */
