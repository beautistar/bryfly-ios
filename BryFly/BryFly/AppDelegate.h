//
//  AppDelegate.h
//  BryFly
//
//  Created by Developer on 11/22/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UserEntity * Me;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UserEntity *Me;


@end

