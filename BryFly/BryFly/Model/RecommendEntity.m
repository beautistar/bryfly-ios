//
//  RecommendEntity.m
//  BryFly
//
//  Created by Developer on 12/11/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "RecommendEntity.h"

@implementation RecommendEntity

@synthesize _idx, _fName, _lName, _content, _user_idx;

- (instancetype) init {
    
    if (self = [super init]) {
        
        _idx = 0;
        _user_idx = 0;
        _fName = @"";
        _lName = @"";
        _content = @"";
    }
    
    return self;
}

@end
