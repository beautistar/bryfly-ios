//
//  UserDefault.m
//  BryFly
//
//  Created by Developer on 12/8/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "UserDefault.h"

@implementation UserDefault

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
    }
    
    return self;
}

+(void) setBoolValue : (NSString *) keyString value : (BOOL) value {
    
    [USERDEFAULTS setBool:value forKey:keyString];
}

+(BOOL) getBoolValue : (NSString *) keyString {
    
    return [USERDEFAULTS boolForKey:keyString];
}

+(void) setLongValue:(NSString *) keyString value:(NSNumber *) value {
    
    [USERDEFAULTS setValue:value forKey:keyString];
}

+(NSNumber *) getLongValue:(NSString *) keyString {
    
    return [USERDEFAULTS valueForKey:keyString];
}

// set and get method with int
+(void) setIntValue:(NSString *) keyString value:(int) value {
    [USERDEFAULTS setValue:[NSString stringWithFormat:@"%i", value] forKey:keyString];
}

+(int) getIntValue:(NSString *) keyString {
    
    NSString * str = [USERDEFAULTS valueForKey:keyString];
    return [str intValue];
}

// set and get method with string
+(void) setStringValue:(NSString *) keyString value:(NSString*) value {
    
    [USERDEFAULTS setValue:value forKey:keyString];
}

+(NSString *) getStringValue:(NSString *) keyString {
    
    return [USERDEFAULTS valueForKey:keyString];
}

@end
