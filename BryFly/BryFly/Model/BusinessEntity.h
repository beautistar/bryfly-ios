//
//  BusinessEntity.h
//  BryFly
//
//  Created by Developer on 12/9/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessEntity : NSObject

@property (nonatomic) int _idx;
@property (nonatomic) int _user_idx;
@property (nonatomic, strong) NSString *_name;
@property (nonatomic, strong) NSString *_description;
@property (nonatomic, strong) NSString *_contact;
@property (nonatomic, strong) NSString *_category;
@property (nonatomic, strong) NSString *_state;
@property (nonatomic, strong) NSString *_city;
@property (nonatomic, strong) NSString *_special_offer;
@property (nonatomic, strong) NSString *_logo;

@end
