//
//  UserEntity.m
//  BryFly
//
//  Created by Developer on 12/8/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "UserEntity.h"

@implementation UserEntity

@synthesize _idx, _email, _fName, _lName, _password, _remember, _fullName, _myBusiness, _userType;

- (instancetype) init {

    if (self = [super init]) {

        // initalize code here

        _idx = 0;
        _fName = @"";
        _email = @"";
        _lName = @"";
        _remember = NO;
        _password = @"";
        _fullName = @"";
        _userType = 0;
       
        _myBusiness = [[BusinessEntity alloc] init];
    }

    return self;
}

- (NSString *) setFullName:(NSString *) fname lname : (NSString *) lname {
    
    return  [NSString stringWithFormat:@"%@%@%@", _fName, @" ", lname];
}



@end
