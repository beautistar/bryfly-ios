//
//  RecommendEntity.h
//  BryFly
//
//  Created by Developer on 12/11/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecommendEntity : NSObject

@property (nonatomic) int _idx;
    @property (nonatomic) int _user_idx;
@property (nonatomic, strong) NSString *_fName;
@property (nonatomic, strong) NSString *_lName;
@property (nonatomic, strong) NSString * _content;

@end
