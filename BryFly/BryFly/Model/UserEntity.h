//
//  UserEntity.h
//  BryFly
//
//  Created by Developer on 12/8/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BusinessEntity.h"

@interface UserEntity : NSObject

@property (nonatomic) int _idx;
@property (nonatomic, strong) NSString *_fName;
@property (nonatomic, strong) NSString *_lName;
@property (nonatomic, strong) NSString *_email;
@property (nonatomic, strong) NSString *_password;
@property (nonatomic, strong) NSString *_fullName;
@property (nonatomic) int _userType;

@property (nonatomic, strong) BusinessEntity *_myBusiness;

@property (nonatomic) BOOL _remember;

- (NSString *) setFullName:(NSString *) fname lname : (NSString *) lname;

@end
