//
//  BusinessEntity.m
//  BryFly
//
//  Created by Developer on 12/9/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "BusinessEntity.h"

@implementation BusinessEntity

@synthesize _idx, _user_idx, _name, _city, _logo, _state, _contact, _category, _description, _special_offer;

- (instancetype) init {
    
    if (self = [super init]) {
        
        _idx = 0;
        _user_idx = 0;
        _name = @"";
        _description = @"";
        _contact = @"";
        _category = @"";
        _state = @"";
        _city = @"";
        _special_offer = @"";
        _logo = @"";
    }
    
    return self;
}

@end
