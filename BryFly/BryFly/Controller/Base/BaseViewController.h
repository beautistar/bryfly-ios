//
//  BaseViewController.h
//  BryFly
//
//  Created by Developer on 12/4/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface BaseViewController : UIViewController
    
- (void) showLoadingView;
- (void) showLoadingViewWithTitle:(NSString *) title;
- (void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay ;
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative;
- (CGFloat) getOffsetYWhenShowKeybarod;
- (void) registerNotification;
- (void) unregisterNotification;

@end
