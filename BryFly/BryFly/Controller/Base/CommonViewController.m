//
//  CommonViewController.m
//  BryFly
//
//  Created by Developer on 12/8/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CommonViewController.h"

@interface CommonViewController () {
    
    MBProgressHUD *_hud;
}

@end

@implementation CommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showLoadingView {
    
    [self showLoadingViewWithTitle:nil];    
}
- (void) showLoadingViewWithTitle:(NSString *) title {
    
    //    HUD = [[MBProgressHUD alloc] initWithView:view]; //rootVC.navigationController.view
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    //    [self.window addSubview:HUD];
    //    HUD.minSize = CGSizeMake(100.f, 100.f);
    
    // Set the hud to display with a color
    _hud.color = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.8];
    
    _hud.activityIndicatorColor = [UIColor colorWithRed:32/255.0 green:125/255.0 blue:229/25.0 alpha:1.0];
    
    //    HUD.delegate = sender;
    _hud.labelText = title;
    
    //    [HUD show:YES];
    
}
- (void) hideLoadingView {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _hud = nil;

    
}
- (void) hideLoadingView : (NSTimeInterval) delay {
    
    [_hud hide:YES afterDelay:delay];
    _hud = nil;
    
}
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative {
    
        NSDictionary *pinkBoldAttribtes = @{NSForegroundColorAttributeName :[UIColor colorWithRed:32/255.0 green:125/255.0 blue:229/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
        
        NSMutableAttributedString *attributedTitle;
        
        if (title != nil) {
            attributedTitle = [[NSMutableAttributedString alloc] initWithString:title];
            [attributedTitle addAttributes:pinkBoldAttribtes range:NSMakeRange(0, title.length)];
        }
        
        
        NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message];
        [attributedMessage addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:16.0] range:NSMakeRange(0, message.length)];
        
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:title
                                     message:message
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        if(title != nil) {
            [alert setValue:attributedTitle forKey:@"attributedTitle"];
        }
        
        [alert setValue:attributedMessage forKey:@"attributedMessage"];
        
        if(strPositivie != nil) {
            UIAlertAction * yesButton = [UIAlertAction
                                         actionWithTitle:strPositivie
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction * action)
                                         {
                                             //Handel your yes please button action here
                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                         }];
            
            [alert addAction:yesButton];
        }
        
        if(strNegative != nil) {
            UIAlertAction * noButton = [UIAlertAction
                                        actionWithTitle:strNegative
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            //Handel your yes please button action here
                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                        }];
            
            [alert addAction:noButton];
        }
        
        [self presentViewController:alert animated:YES completion:nil];
        //    alert.view.tintColor = [UIColor darkGrayColor];
        //    alert.view.backgroundColor = [UIColor colorWithRed:71/255.0 green:74/255.0 blue:85/255.0 alpha:1.0];
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}



@end
