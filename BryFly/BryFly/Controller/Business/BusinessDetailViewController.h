//
//  BusinessDetailViewController.h
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessEntity.h"
#import "CommonViewController.h"

@interface BusinessDetailViewController : CommonViewController

@property (nonatomic, strong) BusinessEntity *_selectedBusiness;

@end
