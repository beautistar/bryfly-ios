//
//  EditBusinessViewController.h
//  BryFly
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "BaseViewController.h"

#import "BusinessEntity.h"

@interface EditBusinessViewController : BaseViewController

@property (nonatomic, strong) BusinessEntity * _selectedBusiness;

@end
