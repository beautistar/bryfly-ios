//
//  EditBusinessViewController.m
//  BryFly
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "EditBusinessViewController.h"
#import "UITextView+Placeholder.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>
#import "CommonUtils.h"

@interface EditBusinessViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    __weak IBOutlet UITextField *tfName;

    __weak IBOutlet UITextView *tvOffer;
    __weak IBOutlet UITextField *tfCategory;
    __weak IBOutlet UIButton *btnCategory;
    __weak IBOutlet UIButton *btnState;
    __weak IBOutlet UIButton *btnCity;
    __weak IBOutlet UITextField *tfState;
    __weak IBOutlet UITextField *tfCity;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UITextView *tvDescription;
    __weak IBOutlet UITextView *tvContact;
    
    NSArray *categoryList;
    NSArray *stateList;
    NSMutableArray *cityList;
    
    NSString *photoPath;
    
    NSMutableArray *data;
    NSDictionary *jsonDict;
    
    UserEntity * _user;

}

@end

@implementation EditBusinessViewController

@synthesize _selectedBusiness;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initData {
    
    _user = APPDELEGATE.Me;
    
    stateList = [[NSMutableArray alloc] init];
    cityList = [[NSMutableArray alloc] init];
    categoryList = [[NSArray alloc] initWithObjects:@"Aviation", @"Coffee", @"Drinks", @"Lodging", @"Recreation", @"Restaurant", @"Shopping", @"Other", nil];
    
    tvOffer.placeholder = @"Special offer here...";
    tvOffer.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    tvOffer.layer.borderWidth = 0.5f;
    tvOffer.layer.cornerRadius = 3;
    self.title = _selectedBusiness._name;
    
    tvDescription.placeholder = @"Description";
    tvDescription.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    tvDescription.layer.borderWidth = 0.5f;
    tvDescription.layer.cornerRadius = 3;

    tvContact.placeholder = @"Contact Information";
    tvContact.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    tvContact.layer.borderWidth = 0.5f;
    tvContact.layer.cornerRadius = 3;

    // get data from json file
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"distinations" ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    //parse the string inot JSON
    jsonDict = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:NULL];
    
    NSArray *headers_ = [jsonDict allKeys];
    stateList = [headers_ sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    data = [[NSMutableArray alloc] init];
    
    for (NSString *state in stateList)  {
        
        NSMutableArray *sectionArr = [[NSMutableArray alloc] init];
        NSMutableArray* sections = [jsonDict valueForKey:state];
        //NSMutableArray* sections = [[NSMutableArray alloc] init];
        
        //sections = [sections_ sortUsingSelector:@selector(compare:)];
        
        for (NSString * sectionStr in sections) {
            
            [sectionArr addObject:sectionStr];
        }
        
        [data addObject:sectionArr];
    }
    
    // init with selected business
    
    tfName.text = _selectedBusiness._name;
    tvDescription.text = _selectedBusiness._description;
    tvContact.text = _selectedBusiness._contact;
    [imvLogo setImageWithURL:[NSURL URLWithString:_selectedBusiness._logo]];
    tfCategory.text = _selectedBusiness._category;
    tfState.text = _selectedBusiness._state;
    tfCity.text = _selectedBusiness._city;
    tvOffer.text = _selectedBusiness._special_offer;
}

- (IBAction)categoryAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Category" rows:categoryList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfCategory setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnCategory];
}

- (IBAction)stateAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select State" rows:stateList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfState setText:selectedValue];
        
        [cityList removeAllObjects];
        tfCity.text = @"";
        NSArray *cities = [jsonDict valueForKey:selectedValue];
        for (NSString *cityStr in cities) {
            
            [cityList addObject:cityStr];
        }
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnState];
    
}
- (IBAction)cityAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select City" rows:cityList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfCity setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnCity];
}
- (IBAction)setLogoAction:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self camera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    //actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) camera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) gallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                [imvLogo setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    if ([tvOffer isFirstResponder]) {
        
        return 50;
    }
    
    return 250;
}

- (IBAction) updateAction:(id)sender {
    
    if ([self isValid]) {
        
        [self updateBusiness];
    }
}

- (BOOL) isValid {
    
    if (tfName.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_BUSINESSNAME positive:nil negative:ALERT_OK];
        return NO;
    } else if(tvDescription.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_DESCRIPTION positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tvContact.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_CONTACTINFO positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tfCategory.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_CATEGORY positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tfState.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_STATE positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tfCity.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_CITY positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tvOffer.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_SPECIAL_OFFER positive:nil negative:ALERT_OK];
        return NO;
        
//    } else if (photoPath.length == 0) {
//        [self showAlertDialog:ALERT_TITLE message:SELECT_LOGO positive:nil negative:ALERT_OK];
//        return NO;
    }
    
    return YES;
}

- (void) updateBusiness {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPDATEBUSINESS];
    
    NSDictionary * params = @{ID : [NSNumber numberWithInt:_selectedBusiness._idx],
                              NAME : tfName.text,
                              DESCRIPTION : tvDescription.text,
                              CONTACT : tvContact.text,
                              CATEGORY : tfCategory.text,
                              STATE : tfState.text,
                              CITY : tfCity.text,
                              SPECIAL_OFFER : tvOffer.text
                              };

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        NSLog(@"update business : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            if (photoPath.length == 0) {
                
                [self hideLoadingView];
            
                [[Toast makeText:SUCCESS_EDIT duration:2] show];
                
                updatedBusiness = 1;
                
                [self.navigationController popViewControllerAnimated:YES];
                
            } else {
                
                [self updateBusinessLogo];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
    
}

- (void) updateBusinessLogo {
    
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPDATEBUSINESSLOGO];
    
    NSDictionary * params = @{
                              ID : [NSNumber numberWithInt:_selectedBusiness._idx],
                              };
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:photoPath] name:@"file" fileName:@"filename.png" mimeType:@"image/png" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionUploadTask *uploadTask;
    
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:nil
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      
                      if (error) {
                          
                          NSLog(@"Error: %@", error);
                          
                          [self hideLoadingView];
                          [self showAlertDialog:nil message:UPDATE_FAIL positive:ALERT_OK negative:nil];
                      }
                      
                      else {
                          
                          [self hideLoadingView];
                          
                          NSLog(@"edit business logo respond : %@", responseObject);
                          
                          int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                          
                          if(nResultCode == CODE_SUCCESS) {
                              
                              
                              [[Toast makeText:SUCCESS_EDIT duration:2] show];
                              
                              updatedBusiness = 1;
                              
                              [self.navigationController popViewControllerAnimated:YES];

                          }
                      }
                  }];
    
    [uploadTask resume];
}

- (void) gotoLogin {
    
    UINavigationController *loginNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:loginNav];
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
