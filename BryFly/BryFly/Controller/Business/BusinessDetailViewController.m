//
//  BusinessDetailViewController.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "BusinessDetailViewController.h"
#import "EditBusinessViewController.h"

@import AFNetworking;
#import "CommonUtils.h"

@interface BusinessDetailViewController () {
    
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UILabel *lblName;
    __weak IBOutlet UITextField *tfLocation;
    __weak IBOutlet UITextView *tvSpecialOffer;
    __weak IBOutlet UIButton *btnUpdate;
    __weak IBOutlet UITextView *rvDescription;
    __weak IBOutlet UITextView *tvContact;
    
    UserEntity * _user;
    
}

@end

@implementation BusinessDetailViewController

@synthesize _selectedBusiness;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    
    [self setBusiness];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (updatedBusiness == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) setBusiness {
    
    [imvLogo setImageWithURL:[NSURL URLWithString:_selectedBusiness._logo]];
    lblName.text = _selectedBusiness._name;
    rvDescription.text = _selectedBusiness._description;
    tvContact.text = _selectedBusiness._contact;
    tfLocation.text = [NSString stringWithFormat:@"%@, %@", _selectedBusiness._state, _selectedBusiness._city];
    tvSpecialOffer.text = _selectedBusiness._special_offer;
    
    if (_selectedBusiness._user_idx == _user._idx) {
        
        [btnUpdate setHidden:NO];
    } else {
        [btnUpdate setHidden:YES];
    }
}
- (IBAction)updateAction:(id)sender {
    
    EditBusinessViewController *editVC = (EditBusinessViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"EditBusinessViewController"];
    
    editVC._selectedBusiness = _selectedBusiness;
    
    [self.navigationController pushViewController:editVC animated:YES];
    
}



@end
