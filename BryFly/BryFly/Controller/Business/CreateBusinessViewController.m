//
//  CreateBusinessViewController.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CreateBusinessViewController.h"
#import "UITextView+Placeholder.h"

#import <ActionSheetPicker_3_0/ActionSheetPicker.h>

#import "CommonUtils.h"

@interface CreateBusinessViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    __weak IBOutlet UITextField *tfName;

    __weak IBOutlet UITextView *tvOffer;
    __weak IBOutlet UITextField *tfCategory;
    __weak IBOutlet UIButton *btnCategory;
    __weak IBOutlet UIButton *btnState;
    __weak IBOutlet UIButton *btnCity;
    __weak IBOutlet UITextField *tfState;
    __weak IBOutlet UITextField *tfCity;
    __weak IBOutlet UIImageView *imvLogo;
    __weak IBOutlet UITextView *tvDescription;
    __weak IBOutlet UITextView *tvContactInfo;
    
    NSArray *categoryList;
    NSArray *stateList;
    NSMutableArray *cityList;
    
    NSString *photoPath;
    
    NSMutableArray *data;
    NSDictionary *jsonDict;
    
    UserEntity * _user;
    
}

@end

@implementation CreateBusinessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initData {
    
    _user = APPDELEGATE.Me;
    
    stateList = [[NSMutableArray alloc] init];
    cityList = [[NSMutableArray alloc] init];
    categoryList = [[NSArray alloc] initWithObjects:@"Aviation", @"Coffee", @"Drinks", @"Lodging", @"Recreation", @"Restaurant", @"Shopping", @"Other", nil];
    
    tvOffer.placeholder = @"Special offer here...";
    tvOffer.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    tvOffer.layer.borderWidth = 0.5f;
    tvOffer.layer.cornerRadius = 3;
    
    tvDescription.placeholder = @"Description";
    tvDescription.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    tvDescription.layer.borderWidth = 0.5f;
    tvDescription.layer.cornerRadius = 3;
    
    tvContactInfo.placeholder = @"Contact Infomation";
    tvContactInfo.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    tvContactInfo.layer.borderWidth = 0.5f;
    tvContactInfo.layer.cornerRadius = 3;
    
    
    // get data from json file
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"distinations" ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    //parse the string inot JSON
    jsonDict = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:NULL];
    
    NSArray *headers_ = [jsonDict allKeys];
    stateList = [headers_ sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    data = [[NSMutableArray alloc] init];
    
    for (NSString *state in stateList)  {
        
        NSMutableArray *sectionArr = [[NSMutableArray alloc] init];
        NSMutableArray* sections = [jsonDict valueForKey:state];
        //NSMutableArray* sections = [[NSMutableArray alloc] init];
        
        //sections = [sections_ sortUsingSelector:@selector(compare:)];
        
        for (NSString * sectionStr in sections) {
            
            [sectionArr addObject:sectionStr];
        }
        
        [data addObject:sectionArr];
    }
}

- (IBAction)backAction:(id)sender {
    
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)categoryAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Category" rows:categoryList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfCategory setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnCategory];
}

- (IBAction)stateAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select State" rows:stateList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfState setText:selectedValue];
        
        [cityList removeAllObjects];
        tfCity.text = @"";
        NSArray *cities = [jsonDict valueForKey:selectedValue];
        for (NSString *cityStr in cities) {
            
            [cityList addObject:cityStr];
        }
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnState];
    
}
- (IBAction)cityAction:(id)sender {
    
    [self.view endEditing:YES];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select City" rows:cityList initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        [tfCity setText:selectedValue];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        
    } origin:btnCity];
}
- (IBAction)setLogoAction:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self camera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    //actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void) camera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) gallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                [imvLogo setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    if ([tvOffer isFirstResponder]) {
        
        return 50;
    }
    
    return 250;
}

- (IBAction)createAction:(id)sender {
    
    if ([self isValid]) {
        
        [self createBusiness];
    }
}

- (BOOL) isValid {
    
    if (tfName.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_BUSINESSNAME positive:nil negative:ALERT_OK];
        return NO;
    } else if(tvDescription.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_DESCRIPTION positive:nil negative:ALERT_OK];
        return NO;

    } else if(tvContactInfo.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_CONTACTINFO positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tfCategory.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_CATEGORY positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tfState.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_STATE positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tfCity.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_CITY positive:nil negative:ALERT_OK];
        return NO;
        
    } else if(tvOffer.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_SPECIAL_OFFER positive:nil negative:ALERT_OK];
        return NO;
        
    } else if (photoPath.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_LOGO positive:nil negative:ALERT_OK];
        return NO;
    }
    
    return YES;
}

- (void) createBusiness {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_CREATEBUSINESS];
    
    NSDictionary * params = @{USER_ID : [NSNumber numberWithInt:_user._idx],
                              NAME : tfName.text,
                              DESCRIPTION : tvDescription.text,
                              CONTACT : tvContactInfo.text,
                              CATEGORY : tfCategory.text,
                              STATE : tfState.text,
                              CITY : tfCity.text,
                              SPECIAL_OFFER : tvOffer.text
                              };
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:photoPath] name:@"file" fileName:@"filename.png" mimeType:@"image/png" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSURLSessionUploadTask *uploadTask;
    
    uploadTask = [manager
          uploadTaskWithStreamedRequest:request
          progress:nil
          completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
              
              if (error) {
                  
                  NSLog(@"Error: %@", error);
                  
                  [self hideLoadingView];
                  [self showAlertDialog:nil message:UPDATE_FAIL positive:ALERT_OK negative:nil];
              }
              
              else {
                  
                  [self hideLoadingView];
                  
                  NSLog(@"create business respond : %@", responseObject);
                  
                  int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                  
                  if(nResultCode == CODE_SUCCESS) {
                      
                      _user._myBusiness._idx = [[responseObject valueForKey:ID] intValue];
                      _user._myBusiness._user_idx = _user._idx;
                      _user._myBusiness._name = tfName.text;
                      _user._myBusiness._description = tvDescription.text;
                      _user._myBusiness._contact = tvContactInfo.text;
                      _user._myBusiness._category = tfCategory.text;
                      _user._myBusiness._state = tfState.text;
                      _user._myBusiness._city = tfCity.text;
                      _user._myBusiness._special_offer = tvOffer.text;
                      _user._myBusiness._logo = [responseObject valueForKey:FILE_URL];
                      photoPath = @"";
                      
                      [[Toast makeText:SUCCESS_UPDATE duration:2] show];
                      
                      [self gotoLogin];
                  } else if (nResultCode == CODE_EXISTBUNAME) {
                      
                      [self showAlertDialog:nil message:EXIST_BUSINESSNAME positive:ALERT_OK negative:nil];                      
                  } else if (nResultCode == CODE_UPLOADLOGOFAIL) {
                      
                      [self showAlertDialog:nil message:UPDATE_FAIL positive:ALERT_OK negative:ALERT_CANCEL];
                  }
              }
          }];
    
    [uploadTask resume];
}

- (void) gotoLogin {
    
    UINavigationController *loginNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:loginNav];
}
    
@end
