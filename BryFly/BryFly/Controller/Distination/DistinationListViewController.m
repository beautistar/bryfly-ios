//
//  DistinationListViewController.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "DistinationListViewController.h"
#import "DestinationDetailViewController.h"

#import "STCollapseTableView.h"
#import "DestinationHeaderCell.h"
#import "DistinationCell.h"

#import "CommonUtils.h"

@interface DistinationListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    UserEntity * _user;
    
    BOOL searchActive;
    
    NSDictionary *jsonDict;
    NSString *jsonString;
}

@property (weak, nonatomic) IBOutlet STCollapseTableView *tblDistinationList;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *data;
@property (nonatomic, strong) NSMutableArray *headers;

@end

@implementation DistinationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    searchActive = NO;
    
    _data = [[NSMutableArray alloc] init];
    _headers = [[NSMutableArray alloc] init];
    
    [self.tblDistinationList setDataSource:self];
    [self initData];
    
    NSLog(@"username : %@", _user._fullName);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initData {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"distinations" ofType:@"json"];
    jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    jsonDict = [NSJSONSerialization JSONObjectWithData:[jsonString dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:NULL];
    
    [self setDataAll];
    
    //[_tblDistinationList reloadData];
    //[_tblDistinationList openSection:0 animated:NO];
    
    // setup when header tapped, collapse
    [_tblDistinationList setExclusiveSections:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_data count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    DistinationCell* cell = (DistinationCell *)[tableView dequeueReusableCellWithIdentifier:@"DistinationCell"];
    
    NSString* text = [[_data objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    cell.lblName.text = text;
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[_data objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return [self.headers objectAtIndex:section];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString* text = [[_data objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    DestinationDetailViewController *destVC = (DestinationDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"DestinationDetailViewController"];
    
    destVC.selectedDest = text;
    destVC._from = FROM_LIST;
    
    [self.navigationController pushViewController:destVC animated:YES];
    
    NSLog(@"selected cell : %@", text);
    
}

// ---------------------------------------------------------------------------------
#pragma mark - UISearchBarDelegate
// ---------------------------------------------------------------------------------

- (void) searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    searchBar.showsCancelButton = YES;
}

- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
    searchBar.showsCancelButton = NO;
}

- (void) searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
    searchBar.showsCancelButton = NO;
    
    searchBar.text = @"";
    
    [self setDataAll];
    
    if(searchActive) {
        searchActive = NO;
        
        [_tblDistinationList reloadData];
    }
}

- (void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self.view endEditing:YES];
    searchBar.showsCancelButton = NO;
    
    if(searchBar.text.length > 0) {
        [self setSearchData:searchBar.text];
        
        searchActive = YES;
    }
}

- (void) searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if(searchActive && [searchText length] == 0) {
        searchActive = NO;
        [_tblDistinationList reloadData];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (void) setDataAll {
    
    [_data removeAllObjects];
    [_headers removeAllObjects];
    
    //parse the string inot JSON
    NSArray *headers_ = [jsonDict allKeys];
    NSArray * headers = [headers_ sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    for (NSString *header in headers) {
        
        NSMutableArray *sectionArr = [[NSMutableArray alloc] init];
        NSMutableArray* sections = [jsonDict valueForKey:header];
        
        for (NSString * sectionStr in sections) {
            
            [sectionArr addObject:sectionStr];
        }
        
        [_data addObject:sectionArr];
       
    }
    
    for (int i = 0 ; i < [headers count] ; i++)
    {
        UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , 50)];
        [header setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, self.view.frame.size.width - 40, 40)];
        lblName.layer.cornerRadius = 3;
        lblName.text = headers[i];
        lblName.tintColor = [UIColor whiteColor];
        [lblName setTextAlignment:NSTextAlignmentCenter];
        lblName.backgroundColor = [UIColor colorWithRed:3/255.f green:190/255.f blue:255/255.f alpha:1.0];
        lblName.layer.masksToBounds = YES;
        [header addSubview:lblName];
        
        [self.headers addObject:header];
    }
    
    [_tblDistinationList reloadData];
}

- (void) setSearchData:(NSString *) searchString {
    
    [_data removeAllObjects];
    [_headers removeAllObjects];
    NSMutableArray *_searchedHeaders;
    _searchedHeaders = [[NSMutableArray alloc] init];
    
    NSArray *headers_ = [jsonDict allKeys];
    NSArray * headers = [headers_ sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];    
    
    for (NSString *header in headers) {
        
        NSMutableArray *sectionArr = [[NSMutableArray alloc] init];
        NSMutableArray* sections = [jsonDict valueForKey:header];
        
        for (NSString * sectionStr in sections) {
      
            if ([sectionStr rangeOfString:searchString].location != NSNotFound) {
                
                [sectionArr addObject:sectionStr];
            }
        }
        
        if (sectionArr.count > 0) {
        
            [_data addObject:sectionArr];
            [_searchedHeaders addObject:header];
        }

    }
    
    for (int i = 0 ; i < [_searchedHeaders count] ; i++)
    {
        UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width , 50)];
        [header setBackgroundColor:[UIColor whiteColor]];
        
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, self.view.frame.size.width - 40, 40)];
        lblName.layer.cornerRadius = 3;
        lblName.text = _searchedHeaders[i];

        lblName.tintColor = [UIColor whiteColor];
        [lblName setTextAlignment:NSTextAlignmentCenter];
        lblName.backgroundColor = [UIColor colorWithRed:3/255.f green:190/255.f blue:255/255.f alpha:1.0];
        lblName.layer.masksToBounds = YES;
        [header addSubview:lblName];
        
        [self.headers addObject:header];
    }
    
    [_tblDistinationList reloadData];
}

@end
