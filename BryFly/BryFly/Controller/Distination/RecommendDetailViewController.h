//
//  RecommendDetailViewController.h
//  BryFly
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommendEntity.h"
#import "CommonViewController.h"

@interface RecommendDetailViewController : CommonViewController

@property (nonatomic, strong) RecommendEntity * _selectedRecommend;

@end
