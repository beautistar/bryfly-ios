//
//  AddRecommedationViewController.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "AddRecommedationViewController.h"
#import "UITextView+Placeholder.h"
#import "CommonUtils.h"


@interface AddRecommedationViewController () {
    
    __weak IBOutlet UITextView *tvRecommedation;
    
    UserEntity * _user;
    
}

@end

@implementation AddRecommedationViewController

@synthesize _city;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    tvRecommedation.placeholder = @"Please add your recommendation here...";
    tvRecommedation.layer.cornerRadius = 3;
    tvRecommedation.layer.borderWidth = 0.5f;
    tvRecommedation.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitAction:(id)sender {
    
    if (tvRecommedation.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_RECOMMEND positive:nil negative:ALERT_OK];
        return;
    } else {
        
        [self addRecommend];
    }
}

- (void) addRecommend {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_ADDRECOMMEND];
    
    NSDictionary * params = @{USER_ID : [NSNumber numberWithInt:APPDELEGATE.Me._idx],
                              CONTENT : tvRecommedation.text,
                              CITY : _city
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"add recommend respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            [[Toast makeText:SUCCESS_RECOMMEND duration:2] show];
            
            addedRecommend = 1;
            
            [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(gotoBack) userInfo:nil repeats:NO];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}

- (void) gotoBack {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
