//
//  DestinationDetailViewController.h
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface DestinationDetailViewController : CommonViewController

@property (nonatomic, strong) NSString *selectedDest;
@property (nonatomic) int _from;

@end
