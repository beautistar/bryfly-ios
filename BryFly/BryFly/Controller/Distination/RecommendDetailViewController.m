//
//  RecommendDetailViewController.m
//  BryFly
//
//  Created by Developer on 12/14/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "RecommendDetailViewController.h"
#import "DestinationDetailViewController.h"
#import "CommonUtils.h"

@interface RecommendDetailViewController () {
    
    __weak IBOutlet UITextField *tfName;
    __weak IBOutlet UITextView *tvRecommend;
    __weak IBOutlet UIView *actionView;
    
    __weak IBOutlet NSLayoutConstraint *constrainContentBtm;
    UserEntity *_user;
    
}

@end

@implementation RecommendDetailViewController

@synthesize _selectedRecommend;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    
    tvRecommend.layer.cornerRadius = 3;
    tvRecommend.layer.borderWidth = 0.5f;
    tvRecommend.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initData {
    
    if (_user._idx == _selectedRecommend._user_idx) {
        constrainContentBtm.constant = 80.0;
        [actionView setHidden:NO];
    } else {
        constrainContentBtm.constant = 20.0;
        [actionView setHidden:YES];
    }
    
    tfName.text = [NSString stringWithFormat:@"%@ %@", _selectedRecommend._fName, _selectedRecommend._lName];
    tvRecommend.text = _selectedRecommend._content;
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}
- (IBAction)updateAction:(id)sender {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPDATERECOMMEND];
    
    NSDictionary * params = @{ID : [NSNumber numberWithInt:_selectedRecommend._idx],
                              CONTENT : tvRecommend.text,
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"update recommend respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            [[Toast makeText:SUCCESS_UPDATED duration:1.5] show];
            
            addedRecommend = 1;
            
            [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(gotoBack) userInfo:nil repeats:NO];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}

- (void) gotoBack {
    
     [self.navigationController popViewControllerAnimated:YES];
}
    
- (IBAction)deleteAction:(id)sender {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Confirm"
                                 message:@"Are you sure you want to delete?"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    //Add Buttons
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle:@"Yes"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [self deleteRecommend];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:@"Cancel"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   //Handle no, thanks button
                               }];
    
    //Add your buttons to alert controller
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
    
- (void) deleteRecommend {
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_DELETERECOMMEND];
    
    NSDictionary * params = @{ID : [NSNumber numberWithInt:_selectedRecommend._idx],
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"delete recommend respond : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            [[Toast makeText:SUCCESS_UPDATED duration:1.5] show];
            
            addedRecommend = 1;
            
            double delayInSeconds = 2.0;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                NSLog(@"Do some work");
                [self gotoBack];
            });
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];
}

@end
