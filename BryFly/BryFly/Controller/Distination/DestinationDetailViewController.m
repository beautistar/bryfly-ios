//
//  DestinationDetailViewController.m
//  BryFly
//
//  Created by Developer on 11/23/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "DestinationDetailViewController.h"
#import "AddRecommedationViewController.h"
#import "BusinessDetailViewController.h"
#import "RecommendDetailViewController.h"

#import "BusinessCell.h"
#import "RecommendationCell.h"
#import "kxMenu.h"
#import "CommonUtils.h"

@interface DestinationDetailViewController () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    
    
    __weak IBOutlet UITableView *tblBusinessList;
    __weak IBOutlet UICollectionView *cvRecommendationList;
    __weak IBOutlet NSLayoutConstraint *businessCenterConstrain;
    
    UserEntity *_user;
    
    NSMutableArray * _businessList;
    NSMutableArray * _businessListAll;
    NSMutableArray * _recommendList;
}

@end

@implementation DestinationDetailViewController
@synthesize _from;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _businessList = [[NSMutableArray alloc] init];
    _businessListAll = [[NSMutableArray alloc] init];
    _recommendList = [[NSMutableArray alloc] init];
    
    _user = APPDELEGATE.Me;
    self.title = _selectedDest;
    
    tblBusinessList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if ([[UIScreen mainScreen] bounds].size.height == 568.0) {
        
        businessCenterConstrain.constant = 40;
        
    } else {
        
        businessCenterConstrain.constant = 80;
    }
    
    tblBusinessList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self getBusiness];
   
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (addedRecommend == 1 || updatedBusiness == 1) {
        
        addedRecommend = 0;
        updatedBusiness = 0;
    
        [self getBusiness];        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getBusiness {
    
    [_businessList removeAllObjects];
    [_recommendList removeAllObjects];
    [_businessListAll removeAllObjects];
    
    [self showLoadingView];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_GETBUSINESS];
    
    NSDictionary * params = @{
                              CITY : _selectedDest
                              };
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; // text/html request
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer]; //json request
    //        [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];// row request
    
    [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [self hideLoadingView];
        
        NSLog(@"get business : %@", responseObject);
        
        int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
        
        if(nResultCode == CODE_SUCCESS) {
            
            NSDictionary *businessDict = [responseObject valueForKey:RES_BUSINESSINFO];
            
            for (NSDictionary *dict in businessDict) {
                
                BusinessEntity * business = [[BusinessEntity alloc] init];
                
                business._idx = [[dict valueForKey:ID] intValue];
                business._user_idx = [[dict valueForKey:USER_ID] intValue];
                business._name = [dict valueForKey:NAME];
                business._description = [dict valueForKey:DESCRIPTION];
                business._contact = [dict valueForKey:CONTACT];
                business._category = [dict valueForKey:CATEGORY];
                business._state = [dict valueForKey:STATE];
                business._city = [dict valueForKey:CITY];
                business._special_offer = [dict valueForKey:SPECIAL_OFFER];
                business._logo = [dict valueForKey:LOGO_URL];
                
                [_businessList addObject:business];
                [_businessListAll addObject:business];
            }
            
            [tblBusinessList reloadData];
            
            NSDictionary *recommendDict = [responseObject valueForKey:RES_RECOMMENDINFO];
            
            for (NSDictionary *recDict in recommendDict) {
                
                RecommendEntity * _recommend = [[RecommendEntity alloc] init];
                
                _recommend._idx = [[recDict valueForKey:ID] intValue];
                _recommend._user_idx = [[recDict valueForKey:USER_ID] intValue];
                _recommend._fName = [recDict valueForKey:F_NAME];
                _recommend._lName = [recDict valueForKey:L_NAME];
                _recommend._content = [recDict valueForKey:CONTENT];
                
                [_recommendList addObject:_recommend];
            }
            
            [cvRecommendationList reloadData];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"error: %@", error);
        
        [self hideLoadingView];
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
    }];    
}

#pragma mark - tableview

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_businessList count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.f;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BusinessCell *cell = (BusinessCell *)[tableView dequeueReusableCellWithIdentifier:@"BusinessCell"];
    
    [cell setBusinessCell:_businessList[indexPath.row]];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BusinessDetailViewController *detailVC = (BusinessDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"BusinessDetailViewController"];
    detailVC._selectedBusiness = _businessList[indexPath.row];
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - Recommend collectionview

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return _recommendList.count;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 2;
}

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    

    return CGSizeMake(self.view.frame.size.width - 40, 60);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RecommendationCell * rmCell = (RecommendationCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"RecommendationCell" forIndexPath:indexPath];
    
    [rmCell setRecommendCell:_recommendList[indexPath.row]];
    
    return rmCell;
    
}

- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    

    RecommendDetailViewController *recomedDetailVC = (RecommendDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"RecommendDetailViewController"];
    recomedDetailVC._selectedRecommend = _recommendList[indexPath.row];
    
    [self.navigationController pushViewController:recomedDetailVC animated:YES];
}

#pragma mark - Filter menu

// filter menu

- (IBAction)filterAction:(UIButton *)sender {
    
    NSArray *menuItems =
    @[
      
      [KxMenuItem menuItem:@"All"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Aviation"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Coffee"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Drinks"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Lodging"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Recreation"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Restaurant"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Shopping"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
      
      [KxMenuItem menuItem:@"Other"
                     image:nil
                    target:self
                    action:@selector(filterByCategory:)],
    ];
    
    KxMenuItem *first = menuItems[0];
    first.foreColor = [UIColor colorWithRed:246/255.0f green:255/255.0f blue:0/255.0f alpha:1.0];
    first.alignment = NSTextAlignmentCenter;
    
    [KxMenu showMenuInView:self.view
                  fromRect:sender.frame
                 menuItems:menuItems];
}

- (void) filterByCategory : (id) sender {
    
    KxMenuItem *selectedItem = (KxMenuItem *) sender;
    
    NSLog(@"%@", selectedItem.title);
    
    [_businessList removeAllObjects];
    
    for (BusinessEntity * _business in _businessListAll) {
        
        if ([selectedItem.title isEqualToString:@"All"]) {
        
            [_businessList addObject:_business];
        
        } else if ([_business._category isEqualToString:selectedItem.title]) {
            
            [_businessList addObject:_business];
        }
    }
    
    [tblBusinessList reloadData];
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"SegueForAddRecommend"]) {
        
//        if (_user._userType == BUSINESS_USER) {
//            
//            [self showAlertDialog:nil message:CANT_RECOMMEND positive:ALERT_OK negative:nil];
//            return;
//        } else {
        
            AddRecommedationViewController *addRVC = (AddRecommedationViewController *)[segue destinationViewController];
            addRVC._city = _selectedDest;
//        }
    }
}

@end
