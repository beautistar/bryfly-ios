//
//  subscriptionViewController.h
//  BryFly
//
//  Created by Yin on 22/12/2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonViewController.h"

@interface subscriptionViewController : CommonViewController

@property (nonatomic) int type;
@property (nonatomic, strong) NSString * fname;
@property (nonatomic, strong) NSString * lname;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * password;

@end
