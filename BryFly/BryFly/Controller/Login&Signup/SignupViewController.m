//
//  SignupViewController.m
//  BryFly ------
//
//  Created by Developer on 11/22/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SignupViewController.h"
#import "subscriptionViewController.h"
#import <ActionSheetPicker_3_0/ActionSheetPicker.h>
#import "M13Checkbox.h"
#import "CommonUtils.h"
#import "MKStoreKit.h"


@interface SignupViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    
    __weak IBOutlet UIView *monthCheckView;
    __weak IBOutlet UIView *yearCheckView;
    __weak IBOutlet UIImageView *imvProfile;
    __weak IBOutlet UIButton *btnSignup;
    __weak IBOutlet UITextField *tfemail;
    __weak IBOutlet UITextField *tfPassword;
    __weak IBOutlet UITextField *tfFname;
    __weak IBOutlet UITextField *tfLname;
    __weak IBOutlet UIView *coverView;
    __weak IBOutlet NSLayoutConstraint *privacyHConstrain;
    
    NSArray *userTypes;

    NSString *photoPath;
    
    UserEntity *_user;
    
    int user_type;
    
    //BOOL isPurchased;
    __weak IBOutlet UIView *termCheckView;
    
}

@property (nonatomic, strong) M13Checkbox *monthChkBox;
@property (nonatomic, strong) M13Checkbox *yearChkBox;
@property (nonatomic, strong) M13Checkbox *termChkBox;
@end

@implementation SignupViewController
    
    @synthesize _from;

    @synthesize monthChkBox, yearChkBox, termChkBox;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    user_type = 0;
    _user = APPDELEGATE.Me;
    
    monthChkBox = [[M13Checkbox alloc] initWithTitle:nil];
    [monthChkBox setCheckAlignment:M13CheckboxAlignmentLeft];
    monthChkBox.strokeColor = [UIColor blackColor];
    monthChkBox.checkColor = [UIColor blueColor];
    monthChkBox.titleLabel.textColor = [UIColor blackColor];
    
    monthChkBox.tintColor = [UIColor clearColor];
    monthChkBox.frame = CGRectMake(0, 2, monthChkBox.frame.size.width, monthChkBox.frame.size.height);
    [monthChkBox addTarget:self action:@selector(monthCheck:) forControlEvents:UIControlEventValueChanged];
    
    [monthCheckView addSubview:monthChkBox];
    [monthChkBox setCheckState:YES];
    
    yearChkBox = [[M13Checkbox alloc] initWithTitle:nil];
    [yearChkBox setCheckAlignment:M13CheckboxAlignmentLeft];
    yearChkBox.strokeColor = [UIColor blackColor];
    yearChkBox.checkColor = [UIColor blueColor];
    yearChkBox.titleLabel.textColor = [UIColor blackColor];
    
    yearChkBox.tintColor = [UIColor clearColor];
    yearChkBox.frame = CGRectMake(0, 2, yearChkBox.frame.size.width, yearChkBox.frame.size.height );
    [yearChkBox addTarget:self action:@selector(yearCheck:) forControlEvents:UIControlEventValueChanged];
    
    [yearCheckView addSubview:yearChkBox];
    
    termChkBox = [[M13Checkbox alloc] initWithTitle:nil];
    [termChkBox setCheckAlignment:M13CheckboxAlignmentLeft];
    termChkBox.strokeColor = [UIColor blackColor];
    termChkBox.checkColor = [UIColor blueColor];
    termChkBox.titleLabel.textColor = [UIColor blackColor];
    
    termChkBox.tintColor = [UIColor clearColor];
    termChkBox.frame = CGRectMake(0, 2, termChkBox.frame.size.width, termChkBox.frame.size.height );
    [termChkBox addTarget:self action:@selector(termCheck:) forControlEvents:UIControlEventValueChanged];
    
    [termCheckView addSubview:termChkBox];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self initView];
}



- (void) initView {
    
   if (_from == FROM_PILOT) {
        
        privacyHConstrain.constant = 15;
        self.navigationItem.title = @"Pilot";
        [btnSignup setTitle:@"Signup" forState:UIControlStateNormal];
        user_type = COMMON_USER;
        coverView.hidden = NO;
    } else if (_from == FROM_FILGHT){
        
        privacyHConstrain.constant = 15;
        self.navigationItem.title = @"Flight Attendant";
        [btnSignup setTitle:@"Signup" forState:UIControlStateNormal];
        user_type = COMMON_USER;
        coverView.hidden = NO;
    } else {
        coverView.hidden = YES;
        privacyHConstrain.constant = 90;
        self.navigationItem.title = @"Business";
        [btnSignup setTitle:@"Next" forState:UIControlStateNormal];
        user_type = BUSINESS_USER;
    }
    
    imvProfile.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    imvProfile.layer.borderWidth = 1;
    
    yearChkBox.checkState = NO;
    monthChkBox.checkState = YES;
    termChkBox.checkState = NO;    
}

- (void) monthCheck:(M13Checkbox *) _monthChkBox {
    
    if (_monthChkBox.checkState == YES) {
        yearChkBox.checkState = NO;
    } else {
        yearChkBox.checkState = YES;
    }
}

- (void) yearCheck:(M13Checkbox *) _yearChkBox {
    
    if (_yearChkBox.checkState == YES) {
        
        monthChkBox.checkState = NO;
    } else {
        
        monthChkBox.checkState = YES;
    }
}

- (void) termCheck:(M13Checkbox *) _termChkBox {
    
//    if (_termChkBox.checkState == YES) {
//
//        termChkBox.checkState = NO;
//    } else {
//
//        termChkBox.checkState = YES;
//    }
}

- (IBAction)signupAction:(id)sender {
    
    
    if ([self isValid]) {
    
        if (user_type == BUSINESS_USER) {
            
            subscriptionViewController * svc = (subscriptionViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"subscriptionViewController"];
            
            svc.type = monthChkBox.checkState ? 0 : 1;
            svc.fname =  [tfFname.text encodeString:NSUTF8StringEncoding];
            svc.lname = [tfLname.text encodeString:NSUTF8StringEncoding];
            svc.email = [tfemail.text encodeString:NSUTF8StringEncoding];
            svc.password = [[tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] encodeString:NSUTF8StringEncoding];
            [[self navigationController] pushViewController:svc animated:YES];
            
        } else {
            [self signup];
        }
    }
}

- (BOOL) isValid {
    
    if (tfemail.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_EMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (![CommonUtils isValidEmail:tfemail.text]) {
        [self showAlertDialog:ALERT_TITLE message:VALID_EMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfPassword.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_PWD positive:nil negative:ALERT_OK];
        return NO;
    } else if(tfPassword.text.length < 6) {
        [self showAlertDialog:ALERT_TITLE message:VALID_PWD positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfFname.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_USERFNAME positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfLname.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_USERLNAME positive:nil negative:ALERT_OK];
        return NO;
    } else if (!termChkBox.checkState) {
        [self showAlertDialog:ALERT_TITLE message:ACCEPT_TERM positive:nil negative:ALERT_OK];
        return NO;
    }
    
    return YES;
}

- (void) signup {
    
    [self showLoadingView];
    
    NSString *fname =  [tfFname.text encodeString:NSUTF8StringEncoding];
    NSString *lname = [tfLname.text encodeString:NSUTF8StringEncoding];
    NSString *email = [tfemail.text encodeString:NSUTF8StringEncoding];
    NSString *password = [[tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] encodeString:NSUTF8StringEncoding];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@/%@/%@/%d", SERVER_URL, REQ_SIGNUP, fname, lname, email, password, user_type];
    
    NSLog(@"signup request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"signup response url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {

            _user._idx = [[responseObject valueForKey:ID] intValue];
            _user._fName = fname;
            _user._lName = lname;
            [_user setFullName:fname lname:lname];
            _user._password = password;
            
            [CommonUtils saveUserInfo];
        
            [self gotoDestination];

            
        } else if (result_code == CODE_USERNAMERXIST) {
            
            [self showAlertDialog:nil message:EXIST_USERNAME positive:ALERT_OK negative:nil];
            
        } else if (result_code == CODE_EMAILEXSIT) {
            
            [self showAlertDialog:nil message:EXIST_EMAILADDRESS positive:ALERT_OK negative:nil];
            
        }        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
}
- (void) gotoDestination {
    
    UINavigationController *businessNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"distinationNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:businessNav];
    
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)setProfileAction:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self camera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    //actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void) camera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) gallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                [imvProfile setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
