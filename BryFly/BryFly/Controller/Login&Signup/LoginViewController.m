//
//  LoginViewController.m
//  BryFly
//
//  Created by Developer on 11/22/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "LoginViewController.h"
#import "M13Checkbox.h"
#import "CommonUtils.h"

@interface LoginViewController () {
    
    __weak IBOutlet UIView *chbView;
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPassword;
    
    UserEntity *_user;
    
}
    
@property (nonatomic, strong) M13Checkbox *chkbox;

@end

@implementation LoginViewController

    @synthesize chkbox;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    [self initView];
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if ([CommonUtils getUserEmail].length != 0) {
        
        tfEmail.text = [CommonUtils getUserEmail];
        tfPassword.text = [CommonUtils getUserPassword];
    }
    
    if ([CommonUtils getUserRemember]) {        
        
        [chkbox setCheckState:YES];
        
        //[self login];
        
        [self gotoDestinationVC];
    }
}
    
- (void) rememberMe : (id) sender {
        
        
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
- (void) initView {
    
    chkbox = [[M13Checkbox alloc] initWithTitle:nil];
    [chkbox setCheckAlignment:M13CheckboxAlignmentLeft];
    chkbox.strokeColor = [UIColor blackColor];
    chkbox.checkColor = [UIColor blueColor];
    chkbox.titleLabel.textColor = [UIColor blackColor];
    
    chkbox.tintColor = [UIColor clearColor];
    chkbox.frame = CGRectMake(0, 2, chkbox.frame.size.width, chkbox.frame.size.height );
    [chkbox addTarget:self action:@selector(rememberMe:) forControlEvents:UIControlEventValueChanged];
    
    [chbView addSubview:chkbox];
}

- (IBAction)signupAction:(id)sender {
    
    UINavigationController *signupNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"signupNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:signupNav];
    
}

- (BOOL) isValid {
    
    if (tfEmail.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_EMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (![CommonUtils isValidEmail:tfEmail.text]) {
        [self showAlertDialog:ALERT_TITLE message:VALID_EMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfPassword.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_PWD positive:nil negative:ALERT_OK];
        return NO;
    } else if(tfPassword.text.length < 6) {
        [self showAlertDialog:ALERT_TITLE message:VALID_PWD positive:nil negative:ALERT_OK];
        return NO;
    }
    
    return YES;
}


- (IBAction) loginAction:(id)sender {
    
    if ([self isValid]) {
        
        [self login];
    }
}

- (void) login {
    
    [self showLoadingView];
    
    
    NSString *email = [tfEmail.text encodeString:NSUTF8StringEncoding];
    NSString *password = [[tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] encodeString:NSUTF8StringEncoding];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@", SERVER_URL, REQ_LOGIN, email, password];
    
    NSLog(@"login request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"login response url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            NSDictionary *dict = [responseObject valueForKey:RES_USERINFO];
            
            UserEntity * user = [[UserEntity alloc] init];
            
            user._idx = [[dict valueForKey:IDX] intValue];
            user._email = [dict valueForKey:EMAIL];
            user._fName = [dict valueForKey:F_NAME];
            user._lName = [dict valueForKey:L_NAME];
            user._userType = [[dict valueForKey:USER_TYPE] intValue];
            user._password = tfPassword.text;
            user._remember = [chkbox checkState];
            user._fullName = [_user setFullName:_user._fName lname:_user._lName];
            
            APPDELEGATE.Me = user;
            _user = user;
            [CommonUtils saveUserInfo];
            
            [self gotoDestinationVC];
            
        } else if (result_code == CODE_UNREGISTEREDUSER) {            
            [self showAlertDialog:nil message:UNREGISTERED_EMAIL positive:ALERT_OK negative:nil];
        } else if (result_code == CODE_WRONGPWD) {
            [self showAlertDialog:nil message:WRONG_PASSWORD positive:ALERT_OK negative:nil];
        }  else if (result_code == CODE_NON_COMPLETED_BUSINESS) {
            
            NSDictionary *dict = [responseObject valueForKey:RES_USERINFO];
            
            _user._idx = [[dict valueForKey:IDX] intValue];
            _user._email = [dict valueForKey:EMAIL];
            _user._fName = [dict valueForKey:F_NAME];
            _user._lName = [dict valueForKey:L_NAME];
            _user._userType = [[dict valueForKey:USER_TYPE] intValue];
            _user._password = tfPassword.text;
            _user._remember = [chkbox checkState];
            _user._fullName = [_user setFullName:_user._fName lname:_user._lName];
            
            [CommonUtils saveUserInfo];

            [self showAlertDialog:nil message:COMPLETE_BUSINESS positive:ALERT_OK negative:nil];
            
            [self gotoCreateBusiness];
        }

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"Error :%@", error);
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
    
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (void) gotoDestinationVC {
    
    
    UINavigationController *businessNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"distinationNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:businessNav];
    
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    if ([tfPassword isFirstResponder]) {
        
        return 140;
    }
    
    return 220;
}

- (void) gotoCreateBusiness {
    
    UINavigationController *businessNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"busniessNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:businessNav];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
