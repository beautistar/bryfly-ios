//
//  SelectUserTypeViewController.m
//  BryFly
//
//  Created by Developer on 11/22/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SelectUserTypeViewController.h"
#import "SignupViewController.h"

#import "CommonUtils.h"

@interface SelectUserTypeViewController ()

@end

@implementation SelectUserTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginAction:(id)sender {
    
    UINavigationController *loginNavVC = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"loginNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:loginNavVC];
    
}

    
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    SignupViewController *destVC = (SignupViewController *) [segue destinationViewController];
    
    NSLog(@"segue id :%@", [segue identifier]);
    
    if ([[segue identifier] isEqualToString:@"SegueForPilot"]) {
        
        destVC._from = FROM_PILOT;
        
    } else if ([[segue identifier] isEqualToString:@"SegueForFlight"]) {
        
        destVC._from = FROM_FILGHT;
    } else if ([[segue identifier] isEqualToString:@"SegueForBusiness"]) {
        
        destVC._from = FROM_BUSINESS;
    }
}

@end
