//
//  subscriptionViewController.m
//  BryFly
//
//  Created by Yin on 22/12/2017.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "subscriptionViewController.h"
#import "Const.h"
#import "CommonUtils.h"
#import "MKStoreKit.h"

@interface subscriptionViewController () {
    
    BOOL isPurchased;
    UserEntity *_user;
    
}
    @property (weak, nonatomic) IBOutlet UILabel *lblContent;

@end

@implementation subscriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    isPurchased = NO;
    
    [self initStoreKit];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (_type == 0) {
       [_lblContent setText: RENEW_PURCHASE_MONTHLY];
    } else {
        [_lblContent setText: RENEW_PURCHASE_YEARLY];
    }
    
    [self initView];
}

- (void) initStoreKit {
    
    [self showLoadingView];
    
    [[MKStoreKit sharedKit] startProductRequest];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductsAvailableNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init] usingBlock:^(NSNotification *note) {
                                                           
                                                           NSLog(@"Products available: %@", [[MKStoreKit sharedKit] availableProducts]);
                                                           
                                                           dispatch_async(dispatch_get_main_queue(), ^{
                                                               [self hideLoadingView];
                                                           });
                                                       }];
    
}



- (void) initView {
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitProductPurchasedNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      isPurchased = YES;
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self signup];
                                                      });
                                                      NSLog(@"Purchased/Subscribed to product with id: %@", [note object]);
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoredPurchasesNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      NSLog(@"Restored Purchases");
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitRestoringPurchasesFailedNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      
                                                      NSLog(@"Failed restoring purchases with error: %@", [note object]);
                                                  }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitDownloadCompletedNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      NSLog(@"Downloaded product: %@", [note object]);
                                                  }];
    [[NSNotificationCenter defaultCenter] addObserverForName:kMKStoreKitDownloadProgressNotification
                                                      object:nil
                                                       queue:[[NSOperationQueue alloc] init]
                                                  usingBlock:^(NSNotification *note) {
                                                      NSLog(@"Downloading product: %@", [note object]);
                                                  }];
}

- (IBAction)agreeAction:(id)sender {
    
    [self isPaid];
    
}

- (void) isPaid {
    
    //You can check if a product was previously purchased using -isProductPurchased as shown below.
    
    //        if([MKStoreManager isProductPurchased:productIdentifier]) {
    //            //unlock it
    //        }
    //    //You can check for a product's expiry date using -expiryDateForProduct as shown below.
    //
    //        if([MKStoreManager expiryDateForProduct:productIdentifier]) {
    //            //unlock it
    //        }
    //To purchase a feature or to subscribe to a auto-renewing subscription, just call
    
    //    [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:productIdentifier];
    
    if (_type == 0) {
        
        //        MKStoreKit.sharedKit().initiatePaymentRequestForProductWithIdentifier("com.steinlogic.iapdemo.consumable")
        
        //        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"com.steinlogic.iapdemo.consumable"];
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"com.bryfly.plan.basic"];
        
    } else {
        
        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"com.bryfly.plan.premium"];
        //        [[MKStoreKit sharedKit] initiatePaymentRequestForProductWithIdentifier:@"com.steinlogic.iapdemo.nonconsumablenocontent"];
        
    }
}

- (void) signup {
    
    [self showLoadingView];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@/%@/%@/%d", SERVER_URL, REQ_SIGNUP, _fname, _lname, _email, _password, BUSINESS_USER];
    
    NSLog(@"signup request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"signup response url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            _user._idx = [[responseObject valueForKey:ID] intValue];
            _user._fName = _fname;
            _user._lName = _lname;
            [_user setFullName:_fname lname:_lname];
            _user._password = _password;
            
            [CommonUtils saveUserInfo];
            [self gotoCreateBusiness];
            
        } else if (result_code == CODE_USERNAMERXIST) {
            [self hideLoadingView];
            [self showAlertDialog:nil message:EXIST_USERNAME positive:ALERT_OK negative:nil];
            
        } else if (result_code == CODE_EMAILEXSIT) {
            [self hideLoadingView];
            [self showAlertDialog:nil message:EXIST_EMAILADDRESS positive:ALERT_OK negative:nil];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
}

- (void) gotoCreateBusiness {
    
    UINavigationController *businessNav = (UINavigationController *) [self.storyboard instantiateViewControllerWithIdentifier:@"busniessNav"];
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:businessNav];
}

- (IBAction)noAction:(id)sender {
    
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
