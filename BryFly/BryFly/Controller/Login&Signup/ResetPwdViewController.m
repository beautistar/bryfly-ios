//
//  ResetPwdViewController.m
//  BryFly
//
//  Created by Developer on 12/12/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ResetPwdViewController.h"
#import "CommonUtils.h"

@interface ResetPwdViewController () {
    
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfNewPwd;
    
}

@end

@implementation ResetPwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) isValid {
    
    if (tfEmail.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_EMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (![CommonUtils isValidEmail:tfEmail.text]) {
        [self showAlertDialog:ALERT_TITLE message:VALID_EMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfNewPwd.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_PWD positive:nil negative:ALERT_OK];
        return NO;
    } else if(tfNewPwd.text.length < 6) {
        [self showAlertDialog:ALERT_TITLE message:VALID_PWD positive:nil negative:ALERT_OK];
        return NO;
    }
    
    return YES;
}

- (IBAction)resetAction:(id)sender {
    
    if ([self isValid]) {
        
        [self resetPassword];
    }
}


- (void) resetPassword {
    
    [self showLoadingView];

    NSString *email = [tfEmail.text encodeString:NSUTF8StringEncoding];
    NSString *password = [[tfNewPwd.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] encodeString:NSUTF8StringEncoding];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@", SERVER_URL, REQ_RESETPWD, email, password];
    
    NSLog(@"reset password request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"reset password response url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [[Toast makeText:SUCCESS_RESETPWD] show];
            
            [self.navigationController popViewControllerAnimated:YES];
            
        } else if (result_code == CODE_UNREGISTEREDUSER) {
            
            [self showAlertDialog:nil message:UNREGISTERED_EMAIL positive:ALERT_OK negative:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
    
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
